#!/usr/bin/env python
# -*- coding:utf-8 -*-
import pandas
import re


data = pandas.read_csv('titanic.csv', index_col='PassengerId')

sex = data['Sex'].value_counts()
print '1) {} {}'.format(sex['male'], sex['female'])

survivedLine = data['Survived'].value_counts()
survivedPercent = 100.0 * survivedLine[1] / survivedLine.sum()
print '2) удалось выжить: {:0.2f}'.format(survivedPercent)

premiumClassLine = data['Pclass'].value_counts()
premiumClassPersent = 100.0 * premiumClassLine[1] / premiumClassLine.sum()
print '3) пассажиры первого класса: {:0.2f}'.format(premiumClassPersent)

ageLine = data['Age']
print '4) средний возраст: {} и медиана {}'.format(ageLine.mean(), ageLine.median())

correl = data['SibSp'].corr(data['Parch'])
print '5) корреляция Пирсона: {}'.format(correl)

def simpleName(name):
    # Первое слово до запятой - фамилия
    s = re.search('^[^,]+, (.*)', name)
    if s:
        name = s.group(1)


    # Если есть скобки - то имя пассажира в них
    s = re.search('\(([^)]+)\)', name)
    if s:
        name = s.group(1)

    # Удаляем обращения
    name = re.sub('(Miss\. |Mrs\. |Ms\. )', '', name)

    # Берём первое оставшееся слово и убираем кавычки
    name = name.split(' ')[0].replace('"', '')

    return name

names = data[data['Sex'] == 'female']['Name'].map(simpleName)
name_counts = names.value_counts()
print '6) самое популярное женское имя: {}'.format(name_counts.head(1).index.values[0])
