# coding=utf-8
import pandas
import sklearn
from sklearn.cross_validation import KFold, cross_val_score
from sklearn.neighbors import KNeighborsClassifier

import sys
sys.path.append("..")

df = pandas.read_csv('wine.data', header=None)
y = df[0]
X = df.loc[:, 1:]

kf = KFold(len(y), n_folds=5, shuffle=True, random_state=42)

def test_accuracy(kf, X, y):
    scores = list()
    k_range = xrange(1, 51)
    for k in k_range:
        model = KNeighborsClassifier(n_neighbors=k)
        scores.append(cross_val_score(model, X, y, cv=kf, scoring='accuracy'))

    return pandas.DataFrame(scores, k_range).mean(axis=1).sort_values(ascending=False)


accuracy = test_accuracy(kf, X, y)
top_accuracy = accuracy.head(1)

print 'Оптимальное качество при k = {}'.format(top_accuracy.index[0])
print 'Его значение = {}'.format(top_accuracy.values[0])

X = sklearn.preprocessing.scale(X)
accuracy = test_accuracy(kf, X, y)

top_accuracy = accuracy.head(1)

print 'Оптимальное качество после приведения к одному масштабу при k = {}'.format(top_accuracy.index[0])
print 'Его значение = {}'.format(top_accuracy.values[0])
