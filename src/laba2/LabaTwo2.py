# coding=utf-8
import pandas
import sklearn
from numpy import linspace
from sklearn.datasets import load_boston
from sklearn.cross_validation import KFold, cross_val_score
from sklearn.neighbors import KNeighborsRegressor

import sys
sys.path.append("..")

data = load_boston()
X = data.data
y = data.target

X = sklearn.preprocessing.scale(X)

def test_accuracy(kf, X, y):
    scores = list()
    p_range = linspace(1, 10, 200)
    for p in p_range:
        model = KNeighborsRegressor(p=p, n_neighbors=5, weights='distance')
        scores.append(cross_val_score(model, X, y, cv=kf, scoring='mean_squared_error'))

    return pandas.DataFrame(scores, p_range).max(axis=1).sort_values(ascending=False)


kf = KFold(len(y), n_folds=5, shuffle=True, random_state=42)
accuracy = test_accuracy(kf, X, y)

top_accuracy = accuracy.head(1)
print 'Качество оптимальное при p = {}'.format(top_accuracy.index[0])